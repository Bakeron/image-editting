new Vue({
    el: '#imageEdit',
    data: {
      image: '',
      topImage: '10px',
      range: 1,
      frameElement: true,
      heightElement: 300
    },
    methods: {
      onChange(e) { this.createFile(e.target.files[0]); },
      createFile(file) {
        if (!file.type.match('image.*')) { alert('Select an image'); }
        var reader = new FileReader();
        reader.onload = (e) => {this.image = e.target.result; }
        if (file.size > 1000000) alert('Nie idzie wgrać tego pliku, tylko obrazki i maks. 1 MB');
        else reader.readAsDataURL(file);
      }, 
      removeFile() { this.image = ''; },
      clickZoomOut() { 
        if (this.range - 0.5 >= 0.3) this.range -= 0.5; 
      },
      clickZoomIn() {
        if ( Number(this.range) + 0.5 <= 6) this.range = Number(this.range) + 0.5;
      },
      clickFrame() {
        if (this.frameElement) this.frameElement = false;
        else this.frameElement = true;
      },
      defaultElement(el) {
        this.range = 1;
        this.topImage = '0px';
      }
    },
    directives: {
      draggable: function(el, bind, vnode) {
        var startX, startY, initialMouseX, initialMouseY;
        el.style.position = 'absolute';

        function mousemove(e) {
          var mouseX = e.clientX - initialMouseX;
          var mouseY = e.clientY - initialMouseY;
          el.style.top = startY + mouseY + 'px';
          el.style.left = startX + mouseX + 'px';
        }

        function mouseup(e) {
          document.removeEventListener('mousemove', mousemove);
          document.removeEventListener('mouseup', mouseup);
        }

        el.addEventListener('mousedown', function(e) {
          startX = el.offsetLeft;
          startY = el.offsetTop;
          initialMouseX = e.clientX;
          initialMouseY = e.clientY;
          document.addEventListener('mousemove', mousemove);
          document.addEventListener('mouseup', mouseup);
          return false;
        });
      },
      heightElement: function(el, bind) {
        var checkNumber, heightElement, initialMouseY;

        function mousemove(e) {
          var mouseY = e.clientY - initialMouseY;

          if (!isNaN(checkNumber)) heightElement = checkNumber + mouseY;
          else heightElement = 300 + mouseY;

          if (heightElement < 601 && heightElement > 199) el.style.height = heightElement + 'px';
        }

        function mouseup(e) {
          document.removeEventListener('mousemove', mousemove);
          document.removeEventListener('mouseup', mouseup);
          checkNumber = heightElement;
        }

        el.children[1].addEventListener('mousedown', function(e) {
          initialMouseY = e.clientY;
          document.addEventListener('mousemove', mousemove);
          document.addEventListener('mouseup', mouseup);
          return false;
        });

      }
    },
    template: `
      <section id="imageEdit">
        <label v-if="!image" class="btn display-inline">
          SELECT OR DROP AN IMAGE
          <input type="file" name="image" @change="onChange">
        </label>
        <div v-else class="image" :class="{'frameElement': frameElement}" >
          <div v-heightElement class="limit-place" :style="{height: heightElement + 'px'}">
            <img v-draggable :style="{transform: 'matrix(' + range + ', 0, 0, ' + range + ', -1, 6 )', top: topImage}" :src="image" alt="image" class="img" onmousedown="return false" ondragstart="return false"/> 
            <button class="resize"></button>
          </div>
          <div class="buttons">
            <button class="remove" @click="removeFile"></button>
            <button class="default" @click="defaultElement()"></button>
            <button class="zoomOut" @click="clickZoomOut()"></button>
            <div class="range"><input type="range" v-model="range" min="0.3" max="6"></div>
            <button class="zoomIn" @click="clickZoomIn()"></button>
            <button class="frame" @click="clickFrame"><p>Wyłącz/Włącz obramowanie wokół obrazka</p></button>
          </div>
        </div>
      </section>
    `
  });
